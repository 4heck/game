CREATE TABLE public.customer_audits (
   id SERIAL PRIMARY KEY,
   customer_id INT NOT NULL,
   name VARCHAR(40) NOT NULL,
   changed_on TIMESTAMP(6) NOT NULL
)

CREATE OR REPLACE FUNCTION log_name_changes()
  RETURNS trigger AS
$BODY$
BEGIN
   IF NEW.name <> OLD.name THEN
       INSERT INTO customer_audits(customer_id,name,changed_on)
       VALUES(OLD.id,OLD.name,now());
   END IF;
   RETURN NEW;
END;
$BODY$

LANGUAGE plpgsql VOLATILE
COST 100;


CREATE TRIGGER name_changes
  BEFORE UPDATE
  ON customer
  FOR EACH ROW
  EXECUTE PROCEDURE log_name_changes();