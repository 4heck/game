from app import db


class Customer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def to_dictionary(self):
        dictionary = {
            'name': self.name,
        }
        return dictionary


class Owner(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def to_dictionary(self):
        dictionary = {
            'name': self.name,
        }
        return dictionary


class Deal(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner = db.Column(db.String(100))
    customer = db.Column(db.String(100))
    game = db.Column(db.String(100))

    def to_dictionary(self):
        dictionary = {
            'owner': self.owner,
            'customer': self.customer,
            'game': self.game
        }
        return dictionary


class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    duration = db.Column(db.String(100))
    year = db.Column(db.String(100))

    def to_dictionary(self):
        dictionary = {
            'name': self.name,
            'duration': self.duration,
            'year': self.year
        }
        return dictionary
